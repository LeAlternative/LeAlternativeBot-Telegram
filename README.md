<mark><b>ATTENZIONE REPOSITORY SPOSTATO [QUI](https://codeberg.org/lealternative/LeAlternative_Bot_Telegram)</b></mark>

Abbiamo deciso di spostare il repository del bot di Telegram su [Codeberg](https://codeberg.org/lealternative/LeAlternative_Bot_Telegram). Non abbiamo ovviamente nulla contro Gitea né contro [gitea.it](https://gitea.it) ma Codeberg offre [codeberg.page](https://codeberg.page) per pubblicare siti gratuitamente e associati a un repository. Questa cosa ci interessava molto per [À la carte](https://lista.lealternative.net) dunque abbiamo iniziato a creare un account lì.
Successivamente abbiamo deciso di usare Codeberg anche per segnalare problemi ed alternative al sito e infine anche per il codice del bot. Il motivo per queste ultime due scelte è ovviamente il fatto che preferiamo avere (e far creare a chi ci segue) solo un account per i repository e non più di uno, soprattutto per non impazzire.

<mark><b>ATTENZIONE REPOSITORY SPOSTATO [QUI](https://codeberg.org/lealternative/LeAlternative_Bot_Telegram)</b></mark>

### LeAlternativeBot-Telegram

Codice sorgente del bot su Telegram chiamato [@LeAlternativeBot](https://t.me/LeAlternativeBot).

È un bot inline che permette di cercare alternative cercando all'interno di alcuni array creati ad hoc.

Creato anche grazie all'aiuto di @selectallfromdual ([Sito](https://www.selectallfromdual.com/)).

Il bot è scritto in PHP, vorremmo fare alcune modifiche, se qualcuno vuole collaborare è il benvenuto.

**COME CREARE UN PROPRIO BOT CON QUESTO CODICE**

1. Parlare in chat con BotFather su Telegram, premere /newbot e segnarsi l'API che vi verrà assegnata.
2. Sempre in chat con BotFather modificate il bot e abilitate l'opzione chiamata INLINE.
3. Inserite la vostra API all'inizio del file mybot.php dove c'è scritto "INSERIRE TOKEN".
4. Caricare tutti i file in una cartella del vostro web hosting, deve poter essere eseguito il codice PHP (praticamente tutti gli hosting lo permettono).
5. Segnatevi il nome dell'hosting che avete scelto e l'indirizzo del vostro file mybot.php. Facciamo finta sia: www.ilmiosito.it/mybot.php
6. Andate su questo indirizzo, modificando prima le scritte in maiuscolo, per abilitare il webhook: https://api.telegram.org/bot**IL-VOSTRO-TOKEN**/setwebhook?url=**WWW.ILMIOSITO.IT/MYBOT.PHP**
7. Ora il vostro bot è funzionante, all'interno di mybot.php trovate il "cuore" del bot, per modificare o cambiare le frasi modificate frasi.php 

**MODIFICHE E AGGIORNAMENTI DA FARE**

[ ] Il bot deve rispondere a qualunque messaggio gli arrivi e non solo quando gli si scrive la parola /start\
[X] Quando non trova nessun risultato dovrebbe dirlo, magari suggerendo di chiedere aiuto sul gruppo.\
[X] Aggiungere il comando cerca per cercare direttamente all'interno del sito\
↳ [ ] Massimo livello sarebbe che la ricerca con /search fosse anch'essa inline e mostrasse i risultati inline\
[ ] Usare un database al posto dell'array (ha senso o sarebbe solo un peggioramento?)\