<?php

// TELEGRAM BLACKLIST

if ((stripos($text,'t.me/coronavirus_verita') !== false) ||
   (stripos($text,'t.me/davide_zedda') !== false) ||
   (stripos($text,'t.me/unavisioneunavoce') !== false) ||
   (stripos($text,'t.me/evncov') !== false) ||
   (stripos($text,'t.me/Mika Youtubers') !== false) ||
   (stripos($text,'t.me/karposroma') !== false) ||
   (stripos($text,'t.me/infoneash') !== false) ||
   (stripos($text,'t.me/ilviruselostato') !== false) ||
   (stripos($text,'t.me/matteogracis') !== false) ||
   (stripos($text,'t.me/nobufale') !== false) ||
   (stripos($text,'t.me/RadioSavana') !== false) ||
   (stripos($text,'t.me/RadioGregOriginal') !== false) ||
   (stripos($text,'t.me/rossobruni') !== false) ||
   (stripos($text,'t.me/silvanademari') !== false) ||
   (stripos($text,'t.me/tenemy') !== false) ||
   (stripos($text,'t.me/vvvvvinc') !== false) ||
   (stripos($text,'t.me/ViVi idee e strategie') !== false) ||
   (stripos($text,'t.me/Avv_Mauro_Sandri_MODULI') !== false))
	{
	$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
		'text' => "
ciao <b>$username</b>,\nquello che hai linkato è un <b>canale Telegram</b> famoso per <b>non</b> essere una fonte affidabile. La lista dei siti non affidabili è presa da <b><a href='https://www.butac.it/the-black-list'>Butac.it</a></b>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	$telegram->deleteMessage($content);
	}
	
// SITI IN BLACKLIST	
	
if ((stripos($text,'DataBaseItalia.it') !== false) ||
   (stripos($text,'mariagrazias.com/') !== false) ||
   (stripos($text,'musicaeanima.com') !== false) ||
   (stripos($text,'neovitruvian.com') !== false) ||
   (stripos($text,'summit.news') !== false) ||
   (stripos($text,'thelivingspirits.net') !== false) ||
   (stripos($text,'vigilantcitizen.com') !== false) ||
   (stripos($text,'zerohedge.com') !== false) ||
   (stripos($text,'ilmeteo.it') !== false) ||
   (stripos($text,'Frasideilibri.com') !== false) ||
   (stripos($text,'Nonsiamosoli.net') !== false) ||
   (stripos($text,'nogeoingegneria.com') !== false) ||
   (stripos($text,'ricercaindipendente.net') !== false) ||
   (stripos($text,'segnidalcielo.it') !== false) ||
   (stripos($text,'0585News.com') !== false) ||
   (stripos($text,'100giornidaleoni.it') !== false) ||
   (stripos($text,'5stellenews.com') !== false) ||
   (stripos($text,'ADFnews.it') !== false) ||
   (stripos($text,'AGENews.eu') !== false) ||
   (stripos($text,'All-News.tv') !== false) ||
   (stripos($text,'AllNews24.eu') !== false) ||
   (stripos($text,'attaccomirato.com') !== false) ||
   (stripos($text,'Bastabugie.it') !== false) ||
   (stripos($text,'blognews24ore.com') !== false) ||
   (stripos($text,'Borderlinez.com') !== false) ||
   (stripos($text,'Breaknotizie.com') !== false) ||
   (stripos($text,'byoblu.com') !== false) ||
   (stripos($text,'Buonamente.com') !== false) ||
   (stripos($text,'catenaumana.com') !== false) ||
   (stripos($text,'Chesuccede.it') !== false) ||
   (stripos($text,'comedonchisciotte.org') !== false) ||
   (stripos($text,'Condividilo.org') !== false) ||
   (stripos($text,'Corrieredisera.it') !== false) ||
   (stripos($text,'Direttanews.it') !== false) ||
   (stripos($text,'Direttanews24.com') !== false) ||
   (stripos($text,'essere-informati.it') !== false) ||
   (stripos($text,'giornaleilsole.com') !== false) ||
   (stripos($text,'ilgiornaleitaliano.it') !== false) ||
   (stripos($text,'ilgiornalenews.com') !== false) ||
   (stripos($text,'globo365.info') !== false) ||
   (stripos($text,'grandeinganno.it') !== false) ||
   (stripos($text,'Guarda.link') !== false) ||
   (stripos($text,'imolaoggi.it') !== false) ||
   (stripos($text,'nobufale.it') !== false) ||
   (stripos($text,'Perdavvero.com') !== false) ||
   (stripos($text,'PioveGovernoLadro.info') !== false) ||
   (stripos($text,'Sostenitori.info') !== false) ||
   (stripos($text,'ticinolive.ch') !== false) ||
   (stripos($text,'Tutto24.info') !== false) ||
   (stripos($text,'Tuttoinweb.com') !== false) ||
   (stripos($text,'Ultimora24.it') !== false) ||
   (stripos($text,'Videoenotizie.it') !== false) ||
   (stripos($text,'Vocedelweb.com') !== false) ||
   (stripos($text,'Videoviraliweb.com') !== false) ||
   (stripos($text,'Autismovaccini.org') !== false) ||
   (stripos($text,'MedNat.org') !== false) ||
   (stripos($text,'Vivoinsalute.com') !== false) ||
   (stripos($text,'centrometeoitaliano.it') !== false) ||
   (stripos($text,'luogocomune.net') !== false) ||
   (stripos($text,'pandoratv.it') !== false) ||
   (stripos($text,'kontrokultura.it') !== false) ||
   (stripos($text,'viralmagazine.it') !== false))
	{
	$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
		'text' => "
ciao <b>$username</b>,\nquello che hai linkato è un <b>sito</b> famoso per <b>non</b> essere una fonte affidabile. La lista dei siti non affidabili è curata da <b><a href='https://www.butac.it/the-black-list'>Butac.it</a></b>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	$telegram->deleteMessage($content);
	}	
	
?>