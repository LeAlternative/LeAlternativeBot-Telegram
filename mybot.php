<?php
include("Telegram.php");

// Set the bot TOKEN
$bot_id = "TOKEN_DA_INSERIRE";

// Instances the class
$telegram = new Telegram($bot_id);

date_default_timezone_set("Europe/Rome"); 

$data = $telegram->getData();
$query = $data['inline_query']['query'];
$user = $data['inline_query']['from'];
$queryid = $data['inline_query']['id'];
$inline_query_id  	= $telegram->Inline_Query_ID();
$inline_query_text  = $telegram->Inline_Query_Text();
$msgType		  	= $telegram->getUpdateType();
$text 				= $telegram->Text();
$chat_id			= $telegram->ChatID();
$message_id			= $telegram->MessageID();
$username			= $telegram->Username();
$replymessagetext	= $data['message'] [reply_to_message] ['text'];
$userID				= $telegram->UserID();
$grouptitle			= $telegram->messageFromGroupTitle();
$usernamegroup		= $data['message'] [chat] ['username'];
$usernamereply		= $data['message'] [reply_to_message] ['from'] ['username'];
$replymessageid		= $data['message'] [reply_to_message] ['message_id'];

include("frasi.php");

if ($query != null && $query != '' || $query == '')
{

	{
		switch ($query)
		{		
			case '':
				$results = json_encode([['type' => 'article', 'id' => '1', 'title' => 'Manda le istruzioni in chat', 'thumb_url' => 'https://www.lealternative.net/wp-content/uploads/2021/02/question-2519654.png.webp','message_text' => "Questo bot si può utilizzare in due modi:\n\n<b>&#183; Inline:</b> prova a taggare questo bot all'interno di <b>qualunque chat di Telegram</b>. Ti basterà scrivere @LeAlternativeBot <i>testodiricerca</i> e vedrai apparire magicamente i risultati!\nPuoi farlo anche all'interno di un gruppo senza che il bot sia stato invitato: mostrerà a tutti i risultati che hai scelto!\n\n<b>&#183; Cerca:</b> se non trovi nulla nelle risposte rapide fai una ricerca sul sito scrivi direttamente al bot cerca <i>paroladacercare</i>", 'parse_mode' => 'HTML'], ['type' => 'article', 'id' => '2', 'title' => 'Manda in chat i modi per seguire LeAlternative','thumb_url' => 'https://www.lealternative.net/wp-content/uploads/2020/05/cropped-logowhite-blu.png.webp', 'message_text' => "<b>LeAlternative</b> è un progetto ambizioso che vuole aiutarvi a trovare alternative etiche. È adatto a chiunque, non bisogna avere conoscenze tecniche e suggeriamo sempre e solo alternative alla portata di tutti.\n\n🌐 <b>Sito</b>: LeAlternative.net\n📣 <b>Canale</b>: @LeAlternative\n💬 <b>Gruppo</b>: @LeAlternativeGruppoufficiale\n\nProva il nostro <b>bot</b>, scrivi @LeAlternativeBot <i>testodiricerca</i>",'parse_mode' => 'HTML','disable_web_page_preview' => true]]);
				$content = ['inline_query_id' => $queryid, 'results' => $results, 'switch_pm_text' => '✨ Sfoglia qualche alternativa', 'switch_pm_parameter' => '0', 'is_personal' => 'true', 'next_offset' => '', ];
				$reply = $telegram->answerInlineQuery($content);
				break;
		}
	}
}


if ($text == '/start')
	{
	$content = array(
		'chat_id' => $chat_id,
		'text' => "
<b>Ciao,</b>\npremi il tasto qui sotto per provare questo bot all'interno di qualunque chat!\nScrivi /istruzioni invece per capire meglio come funziona questo bot e scoprirne tutte le funzionalità!\n\n\📣 <b>Canale</b>: @LeAlternative\n💬 <b>Gruppo</b>: @LeAlternativeGruppoufficiale\n🤖 Codice sorgente di questo bot: <a href=\"https://gitea.it/LeAlternative/LeAlternativeBot-Telegram\">Gitea.it</a>
",
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Provami','switch_inline_query' => 'Alternative a '],
]
	]]),
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
if ($text == '/istruzioni')
	{
	$content = array(
		'chat_id' => $chat_id,
		'text' => "
Questo bot si può utilizzare in due modi:\n\n<b>&#183; Inline:</b> prova a taggare questo bot all'interno di <b>qualunque chat di Telegram</b>. Ti basterà scrivere @LeAlternativeBot <i>testodiricerca</i> e vedrai apparire magicamente i risultati!\nPuoi farlo anche all'interno di un gruppo senza che il bot sia stato invitato: mostrerà a tutti i risultati che hai scelto!\n\n<b>&#183; Cerca:</b> se non trovi nulla nelle risposte rapide fai una ricerca sul sito scrivi direttamente al bot cerca <i>paroladacercare</i>\n\n📣 <b>Canale</b>: @LeAlternative\n💬 <b>Gruppo</b>: @LeAlternativeGruppoufficiale\n🤖 Codice sorgente di questo bot: <a href=\"https://gitea.it/LeAlternative/LeAlternativeBot-Telegram\">Gitea.it</a>
",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
// CODEBERG
include("codebergpost.php");	

// BLACKLIST
include("blacklist.php");	
	
?>